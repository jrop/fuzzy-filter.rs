use fuzzy_filter::matches;

#[test]
fn test1() {
  assert_eq!(matches("ace", "abcde"), true);
  assert_eq!(matches("ace", "abcd"), false);
}
